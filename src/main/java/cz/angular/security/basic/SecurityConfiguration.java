package cz.angular.security.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by vita on 01.12.14.
 */

@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired
  private TokenStore tokenStore;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
    . sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
      .and()
    .authorizeRequests().antMatchers("/wish").authenticated()
      .and()
    .authorizeRequests().antMatchers("/logou").authenticated()
      .and()
    .exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint())
      .and()

      //TODO currently as controller action, but it could be logout handler
    .logout()
      .logoutUrl("/logout-spring")
      
      .and()
    .csrf().disable();

    http.addFilterBefore(new TokenFilter(tokenStore, authenticationManager()), BasicAuthenticationFilter.class);
  }

  @Bean
  protected AuthenticationEntryPoint unauthorizedEntryPoint() {
    return new AuthenticationEntryPoint() {
      @Override
      public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
      }
    };
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth
      .inMemoryAuthentication()
      .withUser("user").password("password").roles("USER").and()
      .withUser("operator").password("password").roles("OPERATOR", "USER");
  }

}