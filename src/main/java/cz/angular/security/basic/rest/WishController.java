package cz.angular.security.basic.rest;


import cz.angular.security.basic.TokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;
import java.util.*;

@RestController
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WishController {

  List<Wish> wishes = new ArrayList<Wish> ();

  public WishController() {
    wishes.add(new Wish("New MacBook Pro"));
    wishes.add(new Wish("Tesla Model S"));
    wishes.add(new Wish("Sheep wool cutter"));
  }

  @RequestMapping(value = "/info", method = RequestMethod.GET)
  public Map<String, String> getInfo(){

    HashMap<String, String> map = new HashMap<String, String>();

    map.put("Version", "3.1.0");

    return map;
  }

  @Secured ({"ROLE_USER"})
  @RequestMapping(value = "/wishes", method = RequestMethod.GET)
  public List<Wish> getWishes(){
    return wishes;
  }

  @Secured ({"ROLE_OPERATOR"})
  @RequestMapping(value = "/wishes", method= RequestMethod.POST)
  public List<Wish> addWish(@RequestBody Wish wish) {
    wishes.add(wish);

    return wishes;
  }



}