package cz.angular.security.basic.rest;


import cz.angular.security.basic.TokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@EnableGlobalMethodSecurity(securedEnabled = true)
public class ResourceController {

  public final MyResource PUBLIC = new MyResource("public");
  public final MyResource SECURED = new MyResource("secured");

  @Secured ({"ROLE_USER"})
  @RequestMapping(value = "/secured", method= RequestMethod.GET)
  public MyResource getSecured() {
    return SECURED;
  }

  @Secured ({"ROLE_ADMIN"})
  @RequestMapping(value = "/secured/admin", method= RequestMethod.GET)
  public MyResource getSecuredAdmin() {
    return new MyResource("secured-admin");
  }

  private class MyResource {
    private String resource;

    public MyResource(String resource) {
      this.resource = resource;
    }

    public String getResource() {
      return resource;
    }

    public void setResource(String resource) {
      this.resource = resource;
    }
  }


}