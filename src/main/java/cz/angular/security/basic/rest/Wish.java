package cz.angular.security.basic.rest;

/**
 * Created by vita on 07.12.14.
 */
public class Wish {

  private String name;
  private boolean resolved;

  public Wish() {
  }

  public Wish(String wishName) {
    this.name = wishName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isResolved() {
    return resolved;
  }

  public void setResolved(boolean resolved) {
    this.resolved = resolved;
  }
}
