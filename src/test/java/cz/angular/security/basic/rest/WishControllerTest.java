package cz.angular.security.basic.rest;

import cz.angular.security.basic.Application;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:9999")
public class WishControllerTest {

  public static final String LOCALHOST = "http://localhost:9999";

  @Test
  public void infoIsAvailableFree() throws Exception {
    TestRestTemplate rest = new TestRestTemplate();

    ResponseEntity<Map> entity = rest.getForEntity(LOCALHOST + "/info", Map.class);

    assertNotNull(entity.getBody().get("Version"));
  }

  @Test
  @Ignore
  public void wishlistIsNotAvailablePublic() throws Exception {
    RestTemplate rest = new TestRestTemplate();

    ResponseEntity<String> response =
        rest.exchange(
          LOCALHOST + "/wishes",
          HttpMethod.GET,
          new HttpEntity<String>(""),
          String.class);

      assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

}