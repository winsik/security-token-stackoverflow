package cz.angular.security.basic.rest;

import cz.angular.security.basic.Application;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:9999")
public class AuthorizeControllerTest {

  public static final String LOCALHOST = "http://localhost:9999";

  @Test
  public void userCanLoginWithRightPassword() throws Exception {
    RestTemplate rest = new TestRestTemplate();

    Credentials credentials = new Credentials();
    credentials.setName("user");
    credentials.setPassword("password");

    ResponseEntity<UserInfo> response =
      rest.exchange(
        LOCALHOST + "/login",
        HttpMethod.POST,
        new HttpEntity<Credentials>(credentials),
        UserInfo.class);

    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertEquals("user", response.getBody().getName());
    assertNotNull(response.getBody().getToken());
    assertFalse(response.getBody().getRoles().isEmpty());
  }


  @Test
  public void loggedOperatorCanAddToWishlist() throws Exception {
    RestTemplate rest = new TestRestTemplate();

    Credentials credentials = new Credentials();
    credentials.setName("operator");
    credentials.setPassword("password");

    ResponseEntity<UserInfo> response =
      rest.exchange(
        LOCALHOST + "/login",
        HttpMethod.POST,
        new HttpEntity<Credentials>(credentials),
        UserInfo.class);

    String token = response.getBody().getToken();
    assertNotNull(token);

    HttpHeaders tokenWithHeaders = new HttpHeaders();
    tokenWithHeaders.add("X-Auth-Token", token);

    Wish newWish= new Wish("New wish");

    ArrayList<Wish> wishs = new ArrayList<Wish>();

    ResponseEntity<List> wishResponse = rest.exchange(
      LOCALHOST + "/wishes",
      HttpMethod.POST,
      new HttpEntity<Wish>(newWish, tokenWithHeaders),
      List.class);

    List<LinkedHashMap> wishlist = (List<LinkedHashMap>) wishResponse.getBody();
    assertFalse(wishlist.isEmpty());
    assertEquals("New wish", wishlist.get(wishlist.size() - 1).get("name"));

  }


  @Test
  public void loggedUserCanReadWishlist() throws Exception {
    RestTemplate rest = new TestRestTemplate();

    Credentials credentials = new Credentials();
    credentials.setName("user");
    credentials.setPassword("password");

    ResponseEntity<UserInfo> response =
      rest.exchange(
        LOCALHOST + "/login",
        HttpMethod.POST,
        new HttpEntity<Credentials>(credentials),
        UserInfo.class);

    String token = response.getBody().getToken();
    assertNotNull(token);

    HttpHeaders tokenWithHeaders = new HttpHeaders();
    tokenWithHeaders.add("X-Auth-Token", token);

    ResponseEntity<List> wishlist = rest.exchange(
      LOCALHOST + "/wishes",
      HttpMethod.GET,
      new HttpEntity<String>(tokenWithHeaders),
      List.class);

    assertFalse(wishlist.getBody().isEmpty());
  }


  @Test
  public void userWithWrongCreditials() throws Exception {
    RestTemplate rest = new TestRestTemplate();

    Credentials credentials = new Credentials();
    credentials.setName("user");
    credentials.setPassword("password-wrong");

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

    ResponseEntity<Object> response =
      rest.exchange(
        LOCALHOST + "/login",
        HttpMethod.POST,
        new HttpEntity<Credentials>(credentials, headers),
        Object.class);

    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
  }
}